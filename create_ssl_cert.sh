#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
mkdir $DIR/config/certs

cd $DIR/config/certs

# Create cert for Postgres
openssl req -new -text -passout pass:abcd -subj /CN=localhost -out server.req
openssl rsa -in privkey.pem -passin pass:abcd -out server.key
openssl req -x509 -in server.req -text -key server.key -out server.crt
chmod og-rwx server.key

# Create cert for Mongo
openssl req -newkey rsa:2048 -new -x509 -days 3650 -nodes -out mongodb-cert.crt -keyout mongodb-cert.key -subj "/C=CH/ST=Zurich/L=Zurich/O=Rentouch GmbH/CN=www.rentouch.ch"
cat mongodb-cert.key mongodb-cert.crt > mongodb.pem
