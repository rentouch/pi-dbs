How to setup
============

1. Fill out the env files for the containers, so that they know the secrets. They are under ./config/

./conifg/piserver-mongo.env:
MONGO_INITDB_ROOT_USERNAME=pius
MONGO_INITDB_ROOT_PASSWORD=mypw

./config/almsync-mongo.env
MONGO_INITDB_ROOT_USERNAME=pius
MONGO_INITDB_ROOT_PASSWORD=mypw

./config/postgres.env
POSTGRES_USER=pius
POSTGRES_PASSWORD=mypw

2. Create certificates for servers
./create_ssl_cert.sh

2. Start with: docker-compose up -d

